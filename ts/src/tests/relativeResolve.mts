import { Path } from "../index.mjs";


const url1 = './../../root/folder1/folder1.1/folder1.1.1/../../folder1.2.3.4.5.6/folder1.2.3.4.5.6.7/../folder1.2.3.4.5.6.7.8.9/./';
const url2 = './../root/folder1/folder1.2/folder1.2.3/../../folder1.2.3.4.5.6/folder1.2.3.4.5.6.7/../folder1.2.3.4.5.6.7.8.9/../..';

console.log(`\n${url1}`);
console.log('resolution 1:', Path.relativeResolve(url1))

console.log(`\n${url2}`);
console.log('resolution 2:', Path.relativeResolve(url2))
