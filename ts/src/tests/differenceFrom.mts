import { Path } from "../index.mjs";


const url1 = './folder/subfolder-1/subfolder-1.1/.';

const url2 = './folder/subfolder-1/subfolder-1.1/subfolder-1.1.1/.';

const  path1 = new Path(url1);

const  path2 = new Path(url2);


console.log(path1.differenceFrom(new Path(url2)))

console.log(path2.differenceFrom(new Path(url1)))

console.log(path1.differenceFrom(new Path('subfolder-1/subfolder-1.1/subfolder-1.1.1/subfolder-1.1.1.1')))
