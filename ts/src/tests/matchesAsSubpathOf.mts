import { Path } from "../index.mjs";


const path1 = new Path(
    `./folder
    /folder-1
    /folder-1.1/.`
);

console.log(path1
    .matchesAsASubpathOf(
        `./folder
        /folder-1/.`
    )
);


// const path2 = new Path(
//     `./folder
//     /folder-1
//     /folder-1.1`
// );

// console.log(path2
//     .matchesAsASubpathOf(
//         `./folder
//         /folder-1
//         /folder-1.1`
//     )
// ); //=> false


// const path3 = new Path(
//     `./folder-1
//     /folder-1.1`
// );

// console.log(path3
//     .matchesAsASubpathOf(
//         `./folder
//         /folder-1`,
//         false
//     )
// );


// const path4 = new Path(
//     `./folder-1
//     /folder-1.1`
// );

// console.log(path4
//     .matchesAsASubpathOf(
//         `./folder
//         /folder-1
//         /folder-1.1
//         /folder-1.1.1`,
//         false
//     )
// );


// const path5 = new Path(
//     `./folder-1
//     /folder-1.2`
// );

// console.log(path5
//     .matchesAsASubpathOf(
//         `./folder
//         /folder-1
//         /folder-1.1
//         /folder-1.1.1`,
//         false
//     )
// ); //=> false

// const path5 = new Path(
//     `./folder-1
//     /folder-1.1
//     /folder-1.1.1
//     /folder-1.1.1.1`
// );

// console.log(path5
//     .matchesAsASubpathOf(
//         `./folder
//         /folder-1
//         /folder-1.1`,
//         false
//     )
// );

// const path6 = new Path(
//     `./folder-1
//     /folder-1.1
//     /folder-1.1.1`
// );

// console.log(path6
//     .matchesAsASubpathOf(
//         `./folder
//         /folder-1
//         /folder-1.1`,
//         false
//     )
// );
