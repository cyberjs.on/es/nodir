import { Path } from "../index.mjs";


const url = '.\\folder-1\\subfolder-1';
const url2 = './folder-1//subfolder-1/';

const operatingSystemPathSeparator = '\\';

let path: Path;

console.log(Path.normalize(url2
    , {
        operatingSystemSeparator: operatingSystemPathSeparator
    }
));

// console.log(Path.normalize(url
//     , {
//         operatingSystemSeparator: operatingSystemPathSeparator
//     }
// ));

// console.log(Path.normalize(url,
//     {
//         operatingSystemSeparator: operatingSystemPathSeparator,
//         toSeparator: '|'
//     }
// ));

// Path.operatingSystemPathSeparator = operatingSystemPathSeparator;

// console.log(Path.normalize(url));

// path = new Path(url);
// console.log(path.toString());
