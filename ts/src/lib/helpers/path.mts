import { Path as _Path } from '../index.mjs';


export class Path {

    static countRelativeParentsNotationFrom(
        url: string,
        fromIndex: number = 0
    ) {

        const foldernames = _Path.normalize(url).split(_Path.separator);

        let count = 0;

        let index = foldernames.indexOf('..', fromIndex);

        while (foldernames[index] === '..') {
            count++;
            index++;
        }

        return count;
    }

    static removeCurrentPathNotationAtEnd(url: string, toSeparator = _Path.separator) {

        const _pathname = _Path.normalize(url);

        let escapedSeparatorArgPattern = `\\${toSeparator}`;

        if (this.#hasCurrentPathNotationAtEnd(url, toSeparator)) {
            return _pathname.replace(
                new RegExp(
                    `(${escapedSeparatorArgPattern}\\.${escapedSeparatorArgPattern}*)+$`
                )
                , ''
            )
        }

        return _pathname;
    }

    static removeCurrentPathNotationAtBeginning(url: string, toSeparator = _Path.separator) {

        const _pathname = _Path.normalize(url);

        let escapedSeparatorArgPattern = `\\${toSeparator}`;

        if (this.#hasCurrentPathNotationAtBeginning(url, toSeparator)) {
            return _pathname.replace(
                new RegExp(
                    `^(\\.${escapedSeparatorArgPattern})+`,
                    ''
                )
                , ''
            )
        }

        return _pathname;
    }

    static captureCurrentPathNotationAtEnd(url: string, toSeparator = _Path.separator) {

        const result = this.#hasCurrentPathNotationAtEnd(url, toSeparator);

        if (result) {
            return result[1];
        }

        return null;
    }

    static captureCurrentPathNotationAtBeginning(url: string, toSeparator = _Path.separator) {

        const result = this.#hasCurrentPathNotationAtBeginning(url, toSeparator);

        if (result) {
            return result[1];
        }

        return null;
    }

    static #hasCurrentPathNotationAtEnd(url: string, toSeparator = _Path.separator) {

        const _pathname = _Path.normalize(url);

        let escapedSeparatorArgPattern = `\\${toSeparator}`;

        return _pathname.match(
            new RegExp(`(${escapedSeparatorArgPattern}\\.${escapedSeparatorArgPattern}*)$`)
        );
    }

    static #hasCurrentPathNotationAtBeginning(url: string, toSeparator = _Path.separator) {

        const _pathname = _Path.normalize(url);

        let escapedSeparatorArgPattern = `\\${toSeparator}`;

        return _pathname.match(
            new RegExp(`^(\\.${escapedSeparatorArgPattern})`)
        );
    }

}
