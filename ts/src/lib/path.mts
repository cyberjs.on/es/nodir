import { Helpers } from "./helpers/namespace.mjs";
import { PathSeparatorOption } from "./types/index.mjs";


export class Path {

    #_name: string;

    #_directoryname: string;

    #_extension: string | null;

    #_basename: string | null;

    #_pieces: string[];

    static #_operatingSystemPathSeparator: string;

    static #_separator = '/';

    constructor(url: string) {

        const _self = Path;

        _self.#throwErrorIfSeparatorWasnotDefined();

        this.#_name = _self.normalize(url);

        this.#_directoryname = this.#resolveDirectoryname();

        this.#_basename = this.#resolveBasename(this.#_name);

        this.#_extension = this.#resolveExtension(this.#_name);

        this.#_pieces = this.#_name.split(_self.#_separator);
    }

    attachNewParent(url: string): Path {

        return new Path(Path.join(url, this.#_name));
    }

    attachNew(url: string): Path {

        return new Path(Path.join(this.#_name, url));

    }

    withoutLowerFloors(count: number): string {

        const _self = Path;

        if (count > this.#_pieces.length) {
            count = this.#_pieces.length;
        }

        const resolvedFoldersList = this.#_pieces.slice(0, this.#_pieces.length - count);

        return resolvedFoldersList.join(_self.#_separator);
    }

    withoutUpperFloors(count: number): string {

        const _self = Path;

        if (count > this.#_pieces.length) {
            count = this.#_pieces.length;
        }

        const resolvedFoldersList = this.#_pieces.slice(count, this.#_pieces.length);

        return resolvedFoldersList.join(_self.#_separator);
    }

    differenceFrom(path: Path): string | null {

        const _self = Path;

        const slashAtEndPattern = /\/+$/;

        const originalPathname = Helpers.Path
            .removeCurrentPathNotationAtEnd(Helpers.Path
                .removeCurrentPathNotationAtBeginning(this.#_name))
            .replace(slashAtEndPattern, '')
        ;

        const url = Helpers.Path
            .removeCurrentPathNotationAtEnd(Helpers.Path
                .removeCurrentPathNotationAtBeginning(path.toString()))
            .replace(slashAtEndPattern, '')
        ;

        const originalPathFoldersname = originalPathname.split(_self.#_separator);

        const pathFoldersname = url.split(_self.#_separator);

        const originalPathFoldersLength = originalPathFoldersname.length;

        const pathFoldersLength = pathFoldersname.length;

        let pathnameDifference = '';

        let indexOfMajor = 0;

        let minorPathFoldersname;

        let majorPathFoldersname;

        if (pathFoldersLength === originalPathFoldersLength) {
            return null;
        }

        if (pathFoldersLength > originalPathFoldersLength) {
            majorPathFoldersname = pathFoldersname;
            minorPathFoldersname = originalPathFoldersname;
        } else {
            majorPathFoldersname = originalPathFoldersname;
            minorPathFoldersname = pathFoldersname;
        }

        for (let i = 0; i < minorPathFoldersname.length; i++) {
            if (minorPathFoldersname[i] !== majorPathFoldersname[i]) {
                return null;
            }
        }

        indexOfMajor = minorPathFoldersname.length;

        pathnameDifference = majorPathFoldersname.slice(indexOfMajor)
            .join(_self.#_separator);

        return pathnameDifference;
    }

    matchesAsASubpathOfList(parentPathnames: string[], isAbsolute?: boolean): boolean {

        for (let url of parentPathnames) {
            if (this.matchesAsASubpathOf(url, isAbsolute)) {
                return true;
            }
        }

        return false;
    }

    matchesAsASubpathOf(parentPathname: string, isAbsolute = true) {

        const _self = Path;

        const originalPathname = Helpers.Path
            .removeCurrentPathNotationAtEnd(Helpers.Path
                .removeCurrentPathNotationAtBeginning(this.#_name));

        const _parentPathname = Helpers.Path
            .removeCurrentPathNotationAtEnd(Helpers.Path
                .removeCurrentPathNotationAtBeginning(_self.normalize(parentPathname)));

        if (originalPathname === _parentPathname) {
            return false;
        }

        const originalPathFoldersname = originalPathname.split(_self.#_separator);

        const parentFoldersname = _parentPathname.split(_self.#_separator);

        const parentFoldersLength = parentFoldersname.length;

        const parentPathname4PatternTest = _self.join(_parentPathname, _self.#_separator);

        const originalPathname4PatternTest = _self.join((originalPathname), _self.#_separator);

        if (isAbsolute) {

            if (originalPathFoldersname.length === parentFoldersLength) {
                return false;
            }

            return (new RegExp(`^${parentPathname4PatternTest}`)).test(originalPathname4PatternTest);
        } else {

            if (originalPathFoldersname[0] === parentFoldersname[parentFoldersLength - 1]) {
                return true
            } else {

                if (originalPathFoldersname.length < parentFoldersLength) {

                    return (new RegExp(originalPathname4PatternTest))
                        .test(parentPathname4PatternTest);
                } else {
                    return this.#matchesAtEndOfSubpath(parentFoldersname);
                }
            }

        }

    }

    toString(): string {
        return this.#_name;
    }

    static join(...urls: string[]): string {

        const treatInvalidValues = (urls: string[]): string[] => {
            return urls.filter(
                url => url && typeof url === 'string'
            );
        };

        const treatSpacedValues = (urls: string[]): string[] => {
            return urls.map(
                url => {

                    let _pathname = Helpers.Path.removeCurrentPathNotationAtBeginning(url);

                    return treatSpacedValue(_pathname);
                }
            );
        };

        const treatSpacedValue = (url: string) => {

            let _pathname = this.normalize(url);

            let currentPathNotationAtBeginning = Helpers.Path
                .captureCurrentPathNotationAtBeginning(_pathname);

            _pathname = Helpers.Path.removeCurrentPathNotationAtBeginning(_pathname);

            if (_pathname.includes(' ')) {
                _pathname = `"${_pathname}"`;
            }

            if (currentPathNotationAtBeginning) {
                _pathname = `${currentPathNotationAtBeginning}${_pathname}`;
            }

            return _pathname;
        };

        const _urls = [treatSpacedValue(urls[0])]
            .concat(
                treatSpacedValues(treatInvalidValues(urls.slice(1, urls.length)))
            );

        return this.normalize(_urls.join(this.#_separator));
    }

    static normalize(
        url: string,
        pathSeparatorOption: PathSeparatorOption = {
            operatingSystemSeparator: this.#_operatingSystemPathSeparator
        }
    ) {

        let escapedSeparator: string;

        let escapedOperatingSystemSeparator: string;

        if (!url) {
            return '';
        }

        url = url.trim();

        if (!pathSeparatorOption.toSeparator) {
            pathSeparatorOption.toSeparator = this.#_separator;
        }

        escapedSeparator = `\\${pathSeparatorOption.toSeparator}`;

        if (pathSeparatorOption.operatingSystemSeparator) {
            if (pathSeparatorOption.operatingSystemSeparator !== pathSeparatorOption.toSeparator) {
                escapedOperatingSystemSeparator = `\\${pathSeparatorOption.operatingSystemSeparator}`;

                /**
                *
                * substitui o separador do sistema pelo separador padrão da classe
                */
                url = url.replace(new RegExp(`${escapedOperatingSystemSeparator}`, 'g'), pathSeparatorOption.toSeparator);
            }

        } else {
            this.#throwErrorIfSeparatorWasnotDefined();
        }

        url = url
            /**
            *
            * remove o excesso de separadores
            */
            .replace(new RegExp(`${escapedSeparator}+`, 'g'), pathSeparatorOption.toSeparator)
            /**
             *
             * remove quebra de texto e espaços entre o nome das pastas e os separadores de
             * caminho
             */
            .replace(new RegExp(`\n[^${escapedSeparator}]*`, 'g'), '')
            /**
             *
             * replace /./ in the middle of the words, to /
             */
            .replace(
                new RegExp(
                    `(?<=.)${escapedSeparator}\\.${escapedSeparator}(\\.${escapedSeparator})*(?=.)`,
                    'g'
                ),
                pathSeparatorOption.toSeparator
            )
        ;

        return url;
    }

    static relativeResolve(url: string): string {

        const relativeParentsNotationPattern = /^((\.\/)*\.\.\/)+/;

        let relativeParentsNotationResult: RegExpMatchArray | null;

        let relativeParentsNotation = '';

        let count = 0;

        let nextIndex = 0;

        let mountedFoldersname: string[];

        let sliceOfFoldersname: string[];

        let mountedPathname: string;

        let sliceOfPathname: string;

        let parentNotationIndex: number;

        let sliceOfPath: Path;

        relativeParentsNotationResult = url.match(relativeParentsNotationPattern);
        if (relativeParentsNotationResult) {
            relativeParentsNotation = relativeParentsNotationResult[0];
            url = url.replace(relativeParentsNotationPattern, '');
        }

        mountedFoldersname = url.split(this.#_separator);

        parentNotationIndex = mountedFoldersname.indexOf('..');

        do {
            mountedPathname = mountedFoldersname.join(this.#_separator);
            count = Helpers.Path.countRelativeParentsNotationFrom(mountedPathname, parentNotationIndex);
            nextIndex = parentNotationIndex + count;
            sliceOfPath = new Path(mountedFoldersname.slice(0, nextIndex).join(this.#_separator));
            sliceOfPathname = sliceOfPath.withoutLowerFloors(count * 2);
            sliceOfFoldersname = sliceOfPathname.split(this.#_separator);
            mountedFoldersname = [...sliceOfFoldersname, ...mountedFoldersname.slice(nextIndex)];
            parentNotationIndex = mountedFoldersname.indexOf('..');
        } while (nextIndex !== -1);

        return `${relativeParentsNotation}${mountedPathname}`;
    }

    #matchesAtEndOfSubpath(parentFoldersname: string[]) {

        const _self = Path;

        const originalPathname = Helpers.Path.removeCurrentPathNotationAtBeginning(this.#_name);

        const originalPathFoldersname = originalPathname.split(_self.#_separator);

        const parentFolderIndex = parentFoldersname.findIndex(
            foldername => foldername === originalPathFoldersname[0]
        );

        const parentFoldersLength = parentFoldersname.length;

        let foldersFromEnd2Match: string[];

        let matchedFolderCount = 0;

        if (parentFolderIndex >= 0) {

            foldersFromEnd2Match = parentFoldersname.slice(parentFolderIndex, parentFoldersLength);

            for (let i of foldersFromEnd2Match.keys()) {
                if (originalPathFoldersname[i] === foldersFromEnd2Match[i]) {
                    matchedFolderCount++;
                }
            }

            if (matchedFolderCount === foldersFromEnd2Match.length) {
                return true;
            }
        }

        return false;
    }

    #resolveExtension(url: string) {
        return this.#resolvePathnameParts(url, /\.([^\/ ]+)$/);
    }

    #resolveDirectoryname() {

        const _self = Path;

        let directoryname = this.#resolvePathnameParts(this.#_name, /([^\/].*(?=\/))/);

        if (!directoryname) {
            directoryname = '.';
            this.#_name = `${directoryname}${_self.#_separator}${this.#_name}`;
        }

        return directoryname;
    }

    #resolveBasename(url: string) {
        return this.#resolvePathnameParts(url, /([^\/]+)$/);
    }

    #resolvePathnameParts(url: string, pattern: RegExp) {

        const _self = Path;

        let matchResult: RegExpMatchArray | null;

        if (url) {
            matchResult = _self.normalize(url).match(pattern);

            if (matchResult) {
                // o resultado será indexado a partir de 1 quando houver grupo capturado
                return matchResult[1];
            }
        }

        return null;
    }

    static #throwErrorIfSeparatorWasnotDefined() {
        if (!this.#_operatingSystemPathSeparator) {
            throw new Error(`You must to define ${this.name}.operatingSystemPathSeparator`);
        }
    }

    get extension(): string | null {
        return this.#_extension;
    }

    get basename(): string | null {
        return this.#_basename;
    }

    get directoryname(): string {
        return this.#_directoryname;
    }

    set url(value: string) {
        if (value) {
            this.#_name = value;
        }
    }

    static get separator(): string {
        return this.#_separator;
    }

    static set operatingSystemPathSeparator(value: string) {
        this.#_operatingSystemPathSeparator = value;
    }

}
