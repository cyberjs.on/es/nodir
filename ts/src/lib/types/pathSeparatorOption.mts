export type PathSeparatorOption = {

    operatingSystemSeparator: string,

    toSeparator?: string

}
